import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

HALF_TAU = 0.084
PHI_STR =  '59 56 31'


def get_z(delta, phi):
    z = phi - delta
    return z


def seconds_to_str_hms(seconds):
    h = round(seconds//3600)
    m = round((seconds//60) % 60)
    s = round(seconds % 60, 1)
    str_hms = ' '.join(map(str, [h, m, s]))
    return str_hms 


def str_hms_to_seconds(val):
    h, m, s = map(float, val.split())
    seconds = h*3600 + m*60 + s
    return seconds


def str_dms_to_radians(val):
    val = val.replace('+', '')
    sign = -1 if ('-' in val) else 1
    d, m, s = map(float, val.split())
    radians = (d + sign*m/60 + sign*s/3600)*np.pi/180
    return radians


def read_data():
    levels_df = pd.read_csv('18-19_levels.dat', sep=' ')

    stars_df = pd.read_csv('18-19.dat', sep=',', dtype=str)
    stars_df.alpha_m = stars_df.alpha_m.apply(str_hms_to_seconds)
    stars_df.alpha_s = stars_df.alpha_s.apply(str_hms_to_seconds)
    stars_df.delta = stars_df.delta.apply(str_dms_to_radians)
    stars_df.MJD = stars_df.MJD.apply(float)
    stars_df.star = stars_df.star.apply(int)

    mask = stars_df.star >= 512 
    correction = stars_df[mask].alpha_m.apply(lambda x: 86400 - x)
    stars_df.loc[mask, "alpha_m"] = correction

    stars_df['T'] = np.nan
    for star in stars_df.star:
        with open('V/s'+str(star)) as file:
            star_moments = file.read()
        star_moments = star_moments.split('\n')[:-1]
        moments_without_date = list(map(lambda x: ' '.join(x.split()[3:]), star_moments))
        moments_in_seconds = list(map(str_hms_to_seconds, moments_without_date))
        stars_df.loc[stars_df.star == star, 'T'] = np.mean(moments_in_seconds)

    levels_df = levels_df.loc[levels_df.full == 1,]
    stars_df = pd.merge(levels_df, stars_df, how ='inner', on =['star'])
    return stars_df


def get_i(left_o, right_o, left_w, right_w):
    i = ((left_w + right_w) - (left_o + right_o))/2 * HALF_TAU
    return i


def get_I(z, delta):
    return np.cos(z)/np.cos(delta)


def get_A(z, delta):
    return np.sin(z)/np.cos(delta)


def fictitious_star(alpha, T, A):
    alpha_T = alpha - T
    mean_alpha_T = np.mean(alpha_T)
    mean_A = np.mean(A)
    return mean_alpha_T, mean_A


def get_a(mean_alpha_T, mean_A, alpha, T, A):
    alpha_T = alpha - T
    a = (mean_alpha_T - alpha_T)/(mean_A - A)
    return a


def mayer_equation(alpha, T, a, A):
    u = alpha - T - a*A
    return u


def main():
    df = read_data()
    df = df.drop([14])
    phi_radians = str_dms_to_radians(PHI_STR)
    df['z'] = list(map(lambda x: get_z(x, phi_radians), df['delta']))
    df['I'] = get_I(df['z'], df['delta'])
    df['i'] = get_i(df['left_o'], df['right_o'], 
                          df['left_w'], df['right_w'])
    df['i*I'] = df['i']*df['I']
    df['mean_T'] = df['T'] + df['i']*df['I']
    df['alpha_m-T'] = df['alpha_m'] - df['mean_T']
    df['A'] = get_A(df['z'], df['delta'])
    zenith_stars = (-0.5 < df['A']) & (df['A'] < 0.3)
    mean_alpha_T, mean_A = fictitious_star(df['alpha_m'][zenith_stars], 
                                           df['mean_T'][zenith_stars],
                                           df['A'][zenith_stars])
    azimuth_stars = df['A'] > 0.58
    df['a'] = get_a(mean_alpha_T, mean_A, df['alpha_m'][azimuth_stars],
                          df['mean_T'][azimuth_stars], df['A'][azimuth_stars])
    mean_a = np.mean(df['a'])
    std_a = np.std(df['a'])
    df['a_mean*A'] = mean_a*df['A']
    df['u'] = mayer_equation(df['alpha_m'], df['mean_T'], mean_a, df['A'])
    mean_u = np.mean(df['u'])
    std_u = np.std(df['u'])
    plt.hist(df['u'])
    plt.show()
    print('mean_alpha_T: ', mean_alpha_T)
    print('mean_A: ', mean_A)
    print('mean_a: ', mean_a)
    print('std_a: ', std_a)
    print('mean_u: ', mean_u)
    print('std_u: ', std_u)
    df['mean_T'] = df['mean_T'].apply(seconds_to_str_hms)
    df['T'] = df['T'].apply(seconds_to_str_hms)
    df['alpha_m'] = df['alpha_m'].apply(seconds_to_str_hms)
    with open('df.tex', 'w') as tex_file:
        tex_file.write(df.to_latex())

if __name__ == '__main__':
    main()
