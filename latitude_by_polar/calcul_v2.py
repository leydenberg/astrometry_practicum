import numpy as np
from math import log10, tan, pi, sin


def micrometr_to_degree(val):
    deg = val[0] + val[1]/60
    deg += val[2]/12
    deg += (val[3]+val[5])/2/60
    deg += (val[4]+val[6])/2/3600
    return deg


def read_data(file_path, n_oberv=4):
    def split_and_float(line):
        return tuple(map(float, line.split()))

    def strip_comments(line):
        return line.split('#')[0].rstrip()

    with open(file_path, 'r', encoding='utf-8') as file:
        row_data = file.read()
    row_lines = row_data.split('\n')
    row_lines_without_comments = filter(lambda x: bool(x),
                                        map(strip_comments, row_lines))
    row_lines_without_comments = list(row_lines_without_comments)

    T = row_lines_without_comments[:n_oberv]
    T.extend(row_lines_without_comments[n_oberv*4:n_oberv*5])
    T = map(split_and_float, T)
    T = np.array(list(map(todegree2, T)))

    lp = row_lines_without_comments[n_oberv:n_oberv*2]
    lp.extend(row_lines_without_comments[n_oberv*5:n_oberv*6])
    lp = list(map(split_and_float, lp))

    I_L = row_lines_without_comments[n_oberv*2:n_oberv*3]
    I_L = map(split_and_float, I_L)
    I_L = np.array(list(map(micrometr_to_degree, I_L)))

    II_L = row_lines_without_comments[n_oberv*3:n_oberv*4]
    II_L = map(split_and_float, II_L)
    II_L = np.array(list(map(micrometr_to_degree, II_L)))

    I_R = row_lines_without_comments[n_oberv*6:n_oberv*7]
    I_R = map(split_and_float, I_R)
    I_R = np.array(list(map(micrometr_to_degree, I_R)))

    II_R = row_lines_without_comments[n_oberv*7:n_oberv*8]
    II_R = map(split_and_float, II_R)
    II_R = np.array(list(map(micrometr_to_degree, II_R)))
    return T, lp, I_L, II_L, I_R, II_R


def todegree(x):
    return(x[0] + x[1]/60 + x[2]*5/60 + x[3]/60+x[4]/3600)


def todex(x, prec=2):
    minutes = x // 60
    secs = x - minutes * 60
    return int(minutes), round(secs, prec)


def reverse(x, prec=2):
    deg = int(x)
    minutes = int((x - int(x)) * 60)
    secs = (x - int(x) - minutes/60) * 3600
    return deg, minutes, round(secs, prec)


def todegree2(x):
    return x[0] + x[1]/60 + x[2]/3600


T, lp, I_L, II_L, I_R, II_R = read_data("obervation_data.dat")

# data
m = 40
tau2 = 1.280
alpha = 2.951583
delta = 89.34925

print("12+alpha-U: ", reverse(12 + alpha))
print("______________________________________________________________________")
t = T - 12 - alpha
print("t: ", [reverse(x) for x in t])
print("______________________________________________________________________")

Lambda = 4595.36
print("sin^2(t/2): ", np.array([sin(x/2/180*pi*15)**2 for x in t]))
ro = np.array([sin(x/2/180*pi*15)**2 for x in t]) * Lambda
print("rho: ", ro)
print("______________________________________________________________________")

# ro = np.array([924.82,972.42, 1044.30, 1071.13, 1113.27, 1148.61, 1188.18, 1213.54])
ctg_dzeta_0 = 1.68
r = ro + ro**2 * ctg_dzeta_0/412530 + ro**3 * (ctg_dzeta_0**2 + 1/3)/(2*206265**2)
print("r: ", *[todex(x) for x in r])
print("______________________________________________________________________")

# print(r/60)
res = []
# print(r)
for x in r:
    res.append(todex(x))
# print(res)

dR = np.array([(x[0]+x[1]-m)*tau2 for x in lp])/3600

II_L = II_L - 180
II_R = II_R + 180


Lc = (I_L + II_L)/2 + dR[0:4]
Rc = (I_R + II_R)/2 + dR[4:8]
Lc = [reverse(x) for x in Lc]
Rc = [reverse(x) for x in Rc]
print("L': ", *Lc)
print("______________________________________________________________________")
print("R': ", *Rc)
print("______________________________________________________________________")

Lc = (I_L + II_L)/2 + dR[0:4]
Rc = (I_R + II_R)/2 + dR[4:8]
L = Lc + r[0:4]/3600
R = Rc - r[4:8]/3600
print("L: ", *[reverse(x) for x in L])
print("______________________________________________________________________")
print("R: ", *[reverse(x) for x in R])
print("______________________________________________________________________")
# print([reverse(x) for x in R])

meanL = sum(L)/len(L)
meanR = sum(R)/len(R)

print("(L): ", reverse(meanL))
print("(R): ", reverse(meanR))

Mz = 1/2 * (meanL+meanR-360)

print("M_z: ", reverse(Mz))


LMz = meanL-Mz
print("(L) - M_z", reverse(LMz))

# L = [reverse(x) for x in L]
# R = [reverse(x) for x in R]
# print(reverse(LMz))
mu = 1.7640
lamda = 1.0
gamma = -64e-4
B = 34.2e-4
refr = 10**(mu + log10(tan(LMz*(pi/180))) + lamda*gamma + B)/3600
print("refr: ", refr*3600)
print("z: ", reverse(LMz + refr))
print("delta: ", reverse(delta))
print("z + delta: ", reverse(LMz + refr + delta))
print("phi: ", reverse(180-(LMz + refr + delta)))
