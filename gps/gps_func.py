import numpy as np

# CONSTANTS
mu = 3.986004418 * 10**14              # geocentric gravitational constant (in m^3 / s^2)
Omega_dot_e = 7.2921151467 * 10**(-5)  # angular velocity of the Earth rotation (in rad/s)
c = 299792458.0                        # velocity of light
a = 6378137.0                          # semi-major axis of WGS-84 ellipsoid
b = 6356752.0                          # semi-minor axis of WGS-84 ellipsoid

def mean_mov(mu, sqrt_A):
    n_0 = np.sqrt(mu) / sqrt_A**3
    return n_0


def moment_emiss(t_obs, P, c):
    t_em = t_obs - P / c
    return t_em


def time_epoch(t_em, t_oe):
    t = t_em - t_oe
    t = t % 604800
    if t > 302400:
        t = t - 604800
    elif t < -302400:
        t = t + 604800
    return t


def correct_mean_mov(n_0, delta_n):
    n = n_0 + delta_n
    return n


def mean_anomaly(M0, n, t):
    M = M0 + n * t
    return M


def ecc_anomaly(M, e):
    E = M
    i = 1
    while i < 10:
        E = M + e * np.sin(E)
        i += 1
    return E


def true_anomaly(e, E):
    nu = 2 * np.arctan(np.sqrt((1 + e) / (1 - e)) * np.tan(E/2))
    if nu < 0:
        nu += 2 * np.pi
    return nu


def arg_lat(nu, omega):
    phi = nu + omega
    return phi


def corrections(phi, C_us, C_rs, C_is,
                C_uc, C_rc, C_ic):
    delta_u = C_us * np.sin(2*phi) + C_uc * np.cos(2*phi)
    delta_r = C_rs * np.sin(2*phi) + C_rc * np.cos(2*phi)
    delta_i = C_is * np.sin(2*phi) + C_ic * np.cos(2*phi)
    return delta_u, delta_r, delta_i


def correct_values(phi, A, e, E, i_0, IDOT, t,
                   delta_u, delta_r, delta_i):
    u = phi + delta_u
    r = A * (1 - e * np.cos(E)) + delta_r
    i = i_0 + delta_i + IDOT * t
    return u, r, i


def coords_orbit(r, u):
    X_orb = r * np.cos(u)
    Y_orb = r * np.sin(u)
    return X_orb, Y_orb


def correct_lon(t, t_oe, Omega_0, Omega_dot, Omega_dot_e):
    OMEGA = Omega_0 + (Omega_dot - Omega_dot_e) * t - Omega_dot_e * t_oe
    return OMEGA


def get_ITRS(i, OMEGA, X_orb, Y_orb):
    X = X_orb * np.cos(OMEGA) - Y_orb * np.cos(i) * np.sin(OMEGA)
    Y = X_orb * np.sin(OMEGA) + Y_orb * np.cos(i) * np.cos(OMEGA)
    Z =                         Y_orb * np.sin(i)
    return X, Y, Z


def distance_correction(P_0, delta_t):
    P = P_0 + c * delta_t
    return P


def Cramer_method(P, X, Y, Z):
    delta_P = np.array([P[i+1] - P[i] for i in range(4)])
    delta_X = np.array([X[i+1] - X[i] for i in range(4)])
    delta_Y = np.array([Y[i+1] - Y[i] for i in range(4)])
    delta_Z = np.array([Z[i+1] - Z[i] for i in range(4)])
    sigma_P = np.array([P[i+1] + P[i] for i in range(4)])
    sigma_X = np.array([X[i+1] + X[i] for i in range(4)])
    sigma_Y = np.array([Y[i+1] + Y[i] for i in range(4)])
    sigma_Z = np.array([Z[i+1] + Z[i] for i in range(4)])
    
    A = (delta_X * sigma_X + delta_Y * sigma_Y + delta_Z * sigma_Z - delta_P * sigma_P) / 2

    D   = np.linalg.det(np.column_stack(delta_X, delta_Y, delta_Z))
    D_1 = np.linalg.det(np.column_stack(      A, delta_Y, delta_Z))
    D_2 = np.linalg.det(np.column_stack(delta_X,       A, delta_Z))
    D_3 = np.linalg.det(np.column_stack(delta_X, delta_Y,       A))

    d_1 = np.linalg.det(np.column_stack(-delta_P, delta_Y, delta_Z))
    d_2 = np.linalg.det(np.column_stack(delta_X, -delta_P, delta_Z))
    d_3 = np.linalg.det(np.column_stack(delta_X, delta_Y, -delta_P))
    
    X_c = D_1 / D
    Y_c = D_2 / D
    Z_c = D_3 / D
    
    a_x = d_1 / D
    a_y = d_2 / D
    a_z = d_3 / D
    
    delta_X_4c = X[3] - X_c
    delta_Y_4c = Y[3] - Y_c
    delta_Z_4c = Z[3] - Z_c
    return a_x, a_y, a_z, delta_X_4c, delta_Y_4c, delta_Z_4c


def get_coef(a_x, a_y, a_z, delta_X_4c, delta_Y_4c, delta_Z_4c, P):
    a_4 = 1 - a_x - a_y - a_z
    b_4 = P[3] + delta_X_4c * a_x + delta_Y_4c * a_y + delta_Z_4c * a_z
    c_4 = - delta_X_4c ** 2 - delta_Y_4c ** 2 - delta_Z_4c ** 2 + P[3] ** 2
    return a_4, b_4, c_4


def clock_correction(a_4, b_4, c_4):
    eps_1 = -b_4 / a_4 + np.sqrt((b_4 ** 2 - a_4 * c_4)) / a_4
    eps_2 = -b_4 / a_4 - np.sqrt((b_4 ** 2 - a_4 * c_4)) / a_4

    delta_t1 = eps_1 / c
    delta_t2 = eps_2 / c
    
    if abs(delta_t1) < 0.0005:
        eps = eps_1
    elif abs(delta_t2) < 0.0005:
        eps = eps_2
    else:
        eps = -1
        print("calculation eps error!")
    return eps


def Cartesian2WGS84(X_c, Y_c, Z_c, eps, a_x, a_y, a_z):
    X = X_c + eps * a_x
    Y = Y_c + eps * a_y
    Z = Z_c + eps * a_z
    return X, Y, Z


def transformation_coef(a, b):

    e_squared = (a ** 2 - b ** 2) / a ** 2
    ec_squared = (a ** 2 - b ** 2) / b ** 2
    return e_squared, ec_squared


def longitude(X, Y):
    lambd = np.arctg(Y / X)
    return lambd


def Theta(X, Y, Z):
    Theta = np.arctg((a * Z) / (b * np.sqrt(X ** 2 + Y ** 2)))
    return Theta


def latitude(X, Y, Z, e_squared, ec_squared, Theta):
    phi = np.arctg((Z + ec_squared * b * np.sin(Theta) ** 3) / 
                   (np.sqrt(X ** 2 + Y ** 2) - 
                    ec_squared * a * np.cos(Theta) ** 3))
    return phi


def todegree(x):
    deg = int(x)
    minutes = int((x - int(x)) * 60)
    secs = (x - int(x) - minutes/60) * 3600
    return deg, minutes, secs
