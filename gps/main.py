import numpy as np
from astropy.time import Time
import gps_func as gpsf


class NavData():
    def __init__(self, sputnik):
        val1 = slice(0, 22)
        val2 = slice(22, 41)
        val3 = slice(41, 60)
        val4 = slice(60, None)
        self.delta_t = float(sputnik[0][val2])
        self.sqrt_A = float(sputnik[2][val4])
        self.t_oe = float(sputnik[3][val1])
        self.DELTA_n = float(sputnik[1][val3])
        self.M0 = float(sputnik[1][val4])
        self.e = float(sputnik[2][val2])
        self.omega = float(sputnik[4][val3])
        self.C_rs = float(sputnik[1][val2])
        self.C_uc = float(sputnik[2][val1])
        self.C_us = float(sputnik[2][val3])
        self.C_ic = float(sputnik[3][val2])
        self.C_is = float(sputnik[3][val4])
        self.C_rc = float(sputnik[4][val2])
        self.i_0 = float(sputnik[4][val1])
        self.IDOT = float(sputnik[5][val1])
        self.OMEGA_0 = float(sputnik[3][val3])
        self.OMEGA_dot = float(sputnik[4][val4])

    def __str__(self):
        string = "delta_t: " + str(self.delta_t)
        string += "\nsqrt_A: " + str(self.sqrt_A)
        string += "\nt_oe: " + str(self.t_oe)
        string += "\nDELTA_n: " + str(self.DELTA_n)
        string += "\nM0: " + str(self.M0)
        string += "\ne: " + str(self.e)
        string += "\nomega: " + str(self.omega)
        string += "\nC_rs: " + str(self.C_rs)
        string += "\nC_uc: " + str(self.C_uc)
        string += "\nC_us: " + str(self.C_us)
        string += "\nC_ic: " + str(self.C_ic)
        string += "\nC_is: " + str(self.C_is)
        string += "\nC_rc: " + str(self.C_rc)
        string += "\ni_0: " + str(self.i_0)
        string += "\nIDOT: " + str(self.IDOT)
        string += "\nOMEGA_0: " + str(self.OMEGA_0)
        string += "\nOMEGA_dot: " + str(self.OMEGA_dot)
        return string


class ObsData():
    def __init__(self, str_obs):
        val1 = slice(0, 14)
        val2 = slice(14, 30)
        val3 = slice(30, 47)
        val4 = slice(47, 48)
        val5 = slice(49, None)
        self.P1 = float(str_obs[val1])
        self.P2 = float(str_obs[val2])
        self.P_phaze = float(str_obs[val3])
        self.power = float(str_obs[val4])
        self.SNR = float(str_obs[val5])
        self.P = np.mean((self.P1, self.P2))

    def check_power(self):
        if self.power < 5:
            print('WARNING! power level of observed signal so bad: ',
                  self.power)


def epoch_to_time(epoch):
    Y, M, D, h, m, s = map(str, epoch)
    time_str = '20' + '-'.join((Y, M, D)) + 'T' + ':'.join((h, m, s))
    time = Time(time_str, format='isot')
    return time


def separate_data_and_header(data_lines):
    for i, line in enumerate(data_lines):
        if 'END OF HEADER' in line:
            offset = i+1
    header = data_lines[:offset]
    data_lines = data_lines[offset:]
    return data_lines, header


def read_nav_data(file_path, sputnik_num, epoch):
    with open(file_path, 'r') as data_file:
        data = data_file.read()
    data_lines = data.split('\n')

    data_lines, header = separate_data_and_header(data_lines)
    
    data_lines = list(map(lambda x: x.replace('D','e'), data_lines))
    lines_per_sputnik = 8
    sputniks_list = [data_lines[x*lines_per_sputnik:(x+1)*lines_per_sputnik]
                     for x in range(0, len(data_lines)//lines_per_sputnik)]

    time = epoch_to_time(epoch)
    min_diff_time = 1e9
    for sputnik in sputniks_list:
        if sputnik[0].split()[0] == str(sputnik_num):
            sputnik_time = epoch_to_time(sputnik[0][:22].split()[1:])
            if abs((sputnik_time - time).jd) < min_diff_time:
                min_diff_time = abs((sputnik_time - time).jd)
                sputnik_for_nav_data = sputnik
    nav_data = NavData(sputnik_for_nav_data)
    return nav_data 


def get_leap_seconds(header):
    for line in header:
        if 'LEAP SECONDS' in line:
            leap_seconds = int(line.split()[0])
    return leap_seconds


def read_obs_data(file_path, sputnik_num, epoch):
    with open(file_path, 'r') as data_file:
        data = data_file.read()
    data_lines = data.split('\n')

    data_lines, header = separate_data_and_header(data_lines)
    leap_seconds = get_leap_seconds(header)

    data_lines = list(map(lambda x: x.replace('D','e'), data_lines))
    
    sputnik_num = str(sputnik_num)
    time = epoch_to_time(epoch)
    for i, line in enumerate(data_lines):
        if 'G' in line:
            obs_time = epoch_to_time(line[:26].split())
            if time == obs_time:
                sputniks_info = line[30:68].replace(" ","").split('G')
                if sputnik_num in sputniks_info[1:]:
                    num_row = sputniks_info.index(sputnik_num)
                    offset = i
                    break

    obs_data = ObsData(data_lines[offset+num_row])
    obs_data.leap_seconds = leap_seconds

    return obs_data


def main():
    epoch_obs = (21, 6, 22, 0, 0, 9)
    sputnik_num = 15

    nd = read_nav_data('javd_20210622.21n', sputnik_num, epoch_obs)
    od = read_obs_data('javd_20210622.21o', sputnik_num, epoch_obs)
    
    print('epoch_obs: ', epoch_obs)
    print('sputnik_num :', sputnik_num)

    print(nd)
    print('P :', od.P)

    t_obs = epoch_to_time(epoch_obs).gps - od.leap_seconds
    print('t_obs, leap_seconds :', t_obs, od.leap_seconds)
    n_0 = gpsf.mean_mov(gpsf.mu, nd.sqrt_A)
    print('n_0 :', n_0)
    t_em = gpsf.moment_emiss(t_obs, od.P, gpsf.c)
    print('t_em :', t_em)
    t = gpsf.time_epoch(t_em, nd.t_oe)
    print('t :', t)
    n = gpsf.correct_mean_mov(n_0, nd.DELTA_n)
    print('n :', n)
    M = gpsf.mean_anomaly(nd.M0, n, t)
    print('M :', M)
    E = gpsf.ecc_anomaly(M, nd.e)
    print('E :', E)
    nu = gpsf.true_anomaly(nd.e, E)
    print('nu :', nu)
    phi = gpsf.arg_lat(nu, nd.omega)
    print('phi :', phi)
    delta_u, delta_r, delta_i = gpsf.corrections(phi, nd.C_us, nd.C_rs,
                                                 nd.C_is, nd.C_uc, nd.C_rc, 
                                                 nd.C_ic)
    print('delta_u, delta_r, delta_i :', delta_u, delta_r, delta_i)
    u, r, i = gpsf.correct_values(phi, nd.sqrt_A**2, nd.e, E, nd.i_0, 
                                  nd.IDOT, t, delta_u, delta_r, delta_i)
    print('u, r, i :', u, r, i)
    X_orb, Y_orb = gpsf.coords_orbit(r, u)
    print('X_orb, Y_orb :', X_orb, Y_orb)
    OMEGA = gpsf.correct_lon(t, nd.t_oe, nd.OMEGA_0, nd.OMEGA_dot, 
                             gpsf.Omega_dot_e)
    print('OMEGA :', OMEGA)
    X, Y, Z = gpsf.get_ITRS(i, OMEGA, X_orb, Y_orb)

    print('X, Y, Z :', X, Y, Z)


if __name__ == '__main__':
    main()
